const tabla = document.querySelector('#lista-exp');
const resumir = {};
['nom','eda','len','dir'].forEach(id =>{
    resumir[id] = document.getElementById(id);
})


function cargaDatos() {
    fetch('http://127.0.0.7:5500/taller.json')
        .then(Response => Response.json())
        .then(json => {

            json.Experiencia.forEach(item => {
                const datos = document.createElement('tr');
                datos.innerHTML +=`
                <td>${item.Empresa}</td>
                <td>${item.Antiguedad}</td>
                
                `;
                tabla.appendChild(datos);
            });

            if(resumir){
                resumir.nom.innerHTML = json.Nombre + " " + json.Apellido;
                resumir.eda.innerHTML = json.Edad;
                resumir.len.innerHTML = json.Lenguaje;
                resumir.dir.innerHTML = json.Direccion;
                
            }



        })
}
cargaDatos();